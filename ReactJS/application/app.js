
import logo from './logo.svg';
import './App.css';

import React from 'react';
import ReactDOM from 'react-dom';

class Base extends React.Component{
  render(){
    return(
      <div className='con'>
      <h1 className='head'>{this.props.name}</h1>
      </div>
    );
  }
}
class Header extends React.Component{
  render(){
    return(
      <div>
        <Base name="Welcome User"/>
      </div>
    );
  }
}
class App extends React.Component{
  render(){
    return(
      <div>
        <Header />
      </div>
    );
  }
}

ReactDOM.render(<App />,document.getElementById('root'));


export default App;