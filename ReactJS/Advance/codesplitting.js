import React from 'react';
import ReactDOM from 'react-dom';

export class Codesplit extends React.Component{
    constructor(props){
        super(props);
        this.name='Monica';
        
    }
    render()
    {
        return(
            <div>
             <Compose name={this.name}/>
            </div>
        );
    }
}
function Compose(props){
    return(
        <h1>{props.name}</h1>
    );
}


//index.js
import {Codesplit} from './codesplitting';


ReactDOM.render(<Codesplit />,document.getElementById('root'));




//React.lazy with suspense
import React from 'react';
export function Lazydemo()
{
    return(
        <div>
            <h1>Hello</h1>
        </div>
    );
}
export default Lazydemo;
 //index.js
 //import Lazydemo from './Lazydemo';

 import React,{Suspense} from 'react';
const Lazydemo=React.lazy(()=>import('./Lazydemo'));
class Reactlazy extends React.Component{
  render()
  {
    return(
      <div>
        <Suspense fallback={<div>Loading...</div>}>
        <Lazydemo />
        </Suspense>
      </div>
    );
  }

}


ReactDOM.render(<Reactlazy />,document.getElementById('root'));

