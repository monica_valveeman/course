import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
function Name(props){

  return (
    <div>
      <p>Welcome {props.name}!!!</p>
    </div>
  );
}
function hoc(Comp){
  return class extends React.Component{
    constructor(props){
      super(props);
    }
    render(){
      return(
        <div>
        <Comp name='User' />
        </div>
      );
    }
  }
}
const NameContainer=hoc(Name);
ReactDOM.render(<NameContainer />,document.getElementById('root'));
