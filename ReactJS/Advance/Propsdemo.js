import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

import PropTypes from 'prop-types';
class Propdemo extends React.Component{
  render()
  {
    return(
      <div>
        <h1>{this.props.fname} {this.props.lname}</h1>
        <p>Age:{this.props.age}</p>
        <p>Gender:{this.props.gender}</p>
      </div>
    );
  }
}
Propdemo.propTypes={
  fname:PropTypes.string,
  lname:PropTypes.string,
  age:PropTypes.number.isRequired,
  gender:PropTypes.oneOf(['Female','Male','Others'])
};
Propdemo.defaultProps={
  fname:'Monica',
  lname:'Valveeman',
  age : 21,
  gender: 'Others'
};
ReactDOM.render(<Propdemo />,document.getElementById('root'));

