import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
class Table extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Hello name='Tony' />   
        <Hello name='Stark' />
        <Hello name='Brave' />
         </React.Fragment>
      
       );
  }
}
function Hello(props){
  return(
    <div>
      <h1>Welcome {props.name}!</h1>

    </div>
    
  );
}
ReactDOM.render(<Table />,document.getElementById('root'));

//shorthand syntax
function Shorthand(props)
{
  return(
  <>
  <h1>User Details</h1>
  <li>First Name: {props.fname}</li>
  <li>Last Name: {props.lname}</li>
  <li>Age: {props.age}</li>
  </>
  );
}
class Columns extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Shorthand fname='Monica'
        lname='Valveeman'
        age='21'  />
        </React.Fragment>);
  }
}
ReactDOM.render(<Columns />,document.getElementById('root'));
