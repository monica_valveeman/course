import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    <li>{number*2}</li>
  );
  return (
    <ul>{listItems}</ul>
  );
}

const numbers = [10,20,30,40,50];
ReactDOM.render(
  <NumberList numbers={numbers} />,
  document.getElementById('root')
);


//square the numbers
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
const num=[2,4,6,8,10];
const square=num.map((num)=><li>{num*num}</li>);


ReactDOM.render(<ul>{square}</ul>,document.getElementById('root'));


//using keys
const num=[2,4,6,8,10];
const square=num.map((num)=><li key={num.toString()}>{num*num}</li>);


ReactDOM.render(<ul>{square}</ul>,document.getElementById('root'));





//keys
import React from 'react';
import reactDom from 'react-dom';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
function Extractkey(props)
{
  return <li>{props.value}</li>;
}
function Listandkeys(props)
{
  const num=props.numbers;
  const list=num.map((num)=>
  <Extractkey key={num.toString()} value={num} />);
return(
  <ul>{list}</ul>
);
}
const numbers=[1,2,3,4,5,6,7,8,9,10];
ReactDOM.render(<Listandkeys numbers={numbers} />,document.getElementById('root'));
