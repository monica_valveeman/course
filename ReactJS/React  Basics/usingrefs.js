//creating ref
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

class Usingref extends React.Component{
  constructor(props){
    super(props);
    this.cref=React.createRef();
  
  }
  render(){
    return(
    <div ref={this.cref}>
     <h1>Welcome to ref and the DOM</h1>
    </div>
    );
  }
}
ReactDOM.render(<Usingref />,document.getElementById('root'));

//Using ref to the dom
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
class Usingref extends React.Component{
    constructor(props){
      super(props);
      this.cref=React.createRef();
      this.inputtext=this.inputtext.bind(this);
    }
    inputtext()
    {
      this.cref.current.focus();
    }
    render(){
      return(
      <div>
  
        <input type="text" ref={this.cref}/>
        <input type="button"  value="Focus the textbox" onClick={this.inputtext}/>
      </div>
      );
    }
  }
  ReactDOM.render(<Usingref />,document.getElementById('root'));
  

//Add refs into class component
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

function CustomInput(props) {  
  let callRefInput = React.createRef();  
   
  function handleClick() {  
    callRefInput.current.focus();  
  }  
   
  return (  
    <div>  
      <h2>Adding Ref to Class Component</h2>  
      <input  
        type="text"  
        ref={callRefInput} />  
      <input  
        type="button"  
        value="Focus input"  
        onClick={handleClick}  
      />  
    </div>  
  );  
}  
class Usingref extends React.Component {  
  constructor(props) {  
    super(props);  
    this.callRefInput = React.createRef();  
  }  
   
  focusRefInput() {  
    this.callRefInput.current.focus();  
  }  
   
  render() {  
    return (  
      <CustomInput ref={this.callRefInput} />  
    );  
  }  
}  
ReactDOM.render(<Usingref />,document.getElementById('root'));
