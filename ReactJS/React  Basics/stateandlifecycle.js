     //using states
            import React from 'react';
            import ReactDOM from 'react-dom';
            class App extends React.Component{
            constructor(props){
                super(props);
                this.state={date:new Date()};
            }
            render()
            {
                return(
                <div>
                    <h1>This is the actual time</h1>
                    <p>The time is {this.state.date.toLocaleTimeString()}</p>
                    </div>
                );
            }
            }
            ReactDOM.render(<App />, document.getElementById('root'));


            import React from 'react';
            import ReactDOM from 'react-dom';

            class App extends React.Component {
            constructor(props) {
                super(props);
                this.state = {
                brand: "Ford",
                model: "Mustang",
                color: "red",
                year: 1964
                };
            }
            changeColor = () => {
                this.setState({color: "blue"});
            }
            render() {
                return (
                <div>
                    <h1>My {this.state.brand}</h1>
                    <p>
                    It is a {this.state.color}   {this.state.model}
                    from {this.state.year}.
                    </p>
                    <button
                    type="button"
                    onClick={this.changeColor}
                    >Change color</button>
                </div>
                );
            }
            }
            ReactDOM.render(<App />, document.getElementById('root'));




                import React from 'react';
                import ReactDOM from 'react-dom';
                class App extends React.Component{
                constructor(props){
                    super(props);
                    this.state={date:new Date()};
                }

                componentDidMount() {
                    this.timerID = setInterval(
                    () => this.tick(),
                    1000
                    );
                }
                
                componentWillUnmount() {
                    clearInterval(this.timerID);
                }

                
                tick() {
                    this.setState({
                    date: new Date()
                    });
                }

                render()
                {
                    return(
                    <div>
                        <h1>This is the actual time</h1>
                        <p>The time is {this.state.date.toLocaleTimeString()}</p>
                        </div>
                    );
                }
                }
                ReactDOM.render(<App />, document.getElementById('root'));



