//Inheritance
import React, { createElement } from 'react';
import reactDom from 'react-dom';
import ReactDOM from 'react-dom';
import './index.css';
//import {NumberList} from './App';
import reportWebVitals from './reportWebVitals';
class UserNameForm extends React.Component {
  render() {
     return (
        <div>
          <label>Enter the user name:</label>
           <input type="text" />
        </div>
     );
  }
}
class CreateUserName extends UserNameForm {
  render() {
     const parent = super.render();
     return (
        <div>
           {parent}
           <button>Create</button>
        </div>
     )
  }
}
class UpdateUserName extends UserNameForm {
  render() {
     const parent = super.render();
     return (
        <div>
           {parent}
           <button>Update</button>
        </div>
     )
  }
}
ReactDOM.render(
  (<div>
     < CreateUserName />
     < UpdateUserName />
  </div>), document.getElementById('root')
);



//composition
class UserNameForm extends React.Component {
    render() {
       return (
          <div>
            <label>Enter the User Name:</label>
             <input type="text" />
          </div>
       );
    }
  }
  class CreateUserName extends React.Component {
    render() {
       return (
          <div>
             < UserNameForm />
             <button>Create</button>
          </div>
       )
    }
  }
  class UpdateUserName extends React.Component {
    render() {
       return (
          <div>
             < UserNameForm />
             <button>Update</button>
          </div>
       )
    }
  }
  ReactDOM.render(
    (<div>
       <CreateUserName />
       <UpdateUserName />
    </div>), document.getElementById('root')
  );
  