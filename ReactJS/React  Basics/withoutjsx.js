
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
class WithoutJSX extends React.Component{
  render()
  {
    return React.createElement('div',null,`This is a ${this.props.name1}`);
  }
}
const element=React.createElement(WithoutJSX,{name1:'Without JSX concept'},null);
ReactDOM.render(element,document.getElementById('root'));
