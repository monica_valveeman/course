function greaterThan(n) {
  return m => m > n;
}
let greaterThan10 = greaterThan(10);
document.write(greaterThan10(11));
// → true


//map 
const arr1 = [1, 2, 3];
const arr2 = arr1.map(function(item) {
  return item * 2;
});
console.log(arr2);