let fs = require("fs");

function readFile(filename) {
    return new Promise(function(resolve, reject) {


        fs.readFile(filename, { encoding: "utf8" }, function(err, contents) {

            if (err) {
                reject(err);
                return;
            }

            resolve(contents);           //read succeed

        });
    });
}

let promise = readFile("link.txt");


promise.then(function(contents) {
    // fulfillment
    console.log(contents);
}, function(err) {
    // rejection
    console.error(err.message);
});

setTimeout(function() {
    console.log("Timeout");
}, 500);

console.log("Hi!");
